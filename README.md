# Manwe Perimeter
Drone Autonomous Mapping and Perimeter Searching.

## Description
This code aims to provide an easy way to coordinate drones using swarm intelligence for the purpose of
maintaining a perimeter around an area, which can alert of intruders, people in need of rescuing, or anything you want. 
The name is  based off of Tolkien's Valar "Manwe" and his great eagles which served as his intelligence and vision in remote areas.

## Components
- Drone Behaviors 
  * Scan / Map Area
  * Patrol (with waypoints)
  * Alert
- SLAM / Sensor Fusion (For mapping and patrolling area)
- GIS Mapping (Plot Drones and recognized objects on GIS or ATAK map)
- Image Processing / Object Recognition (Differentiate between a deer and a person)
- UI (Plot Drones' sensor/camera feeds on one UI like a security cam). Show GIS Map too.
- Drone Swarm Intelligence 
  * Recognize where other drones are, behave accordingly (not too close or far)
  * If Alerting can swarm to a location.

## Roadmap
- Check out interfaces of similar software we could interface with for each of the components, for example OpenDroneMap.
- Implement Components

## Authors and acknowledgment
Chance Cardona, with help from A.

## License
http://www.wtfpl.net/about/

## Project status
Researching